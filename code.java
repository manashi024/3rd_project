package learn; 


class Casio{
    public void sum(int i, int j) {
        System.out.println(i+j);
    }
    public void sum(int i, int j, int k) {
        System.out.println(i+j+k);
    }
    public void sum(double i, double j) {
        System.out.println(i+j);
    }
} 

public class MethodOverloading {     

  public static void main(String[] args) {
        Casio obj = new Casio();
        obj.sum(7,8);
        obj.sum(3,5,6);
        obj.sum(6.8, 8.6);     
  } 

}